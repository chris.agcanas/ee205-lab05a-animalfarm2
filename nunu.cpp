///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   01 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool Native, enum Color newColor, enum Gender newGender){
   isNative = Native;
   species = "Fistularia chinensis";
   gender = newGender;
   scaleColor = newColor;
   favoriteTemp = 80.6;
}

void Nunu::printInfo(){
   cout << "Nunu" << endl;
   // boolalpha prints the bool as a string
   cout << "   Is native = [" << boolalpha << isNative << "]" << endl;
   Fish::printInfo();
}

}
