///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   01 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "palila.hpp"

using namespace std;

namespace animalfarm{

Palila::Palila(string location, enum Color newColor, enum Gender newGender){
   whereFound = location;
   species = "Loxioides bailleui";
   gender = newGender;
   featherColor = newColor;
   isMigratory = false;
}

void Palila::printInfo(){
   cout << "Palila" << endl;
   cout << "   Where Found = [" << whereFound << "]" << endl;
   Bird::printInfo();
}

}
